include::partial$entities.adoc[]

[[sect-hardware-overview]]
= Hardware Overview

Fedora {PRODVER} provides software to suit a wide variety of applications. The storage, memory and processing requirements vary depending on usage. For example, a high traffic database server requires much more memory and storage than a business desktop, which in turn has higher requirements than a single-purpose virtual machine.

[[hardware_overview-specs]]
== Minimum System Configuration

The figures below are a recommended minimum for the default installation. Your requirements may differ, and most applications will benefit from more than the minimum resources.

* 2GHz dual core processor or faster

* 2GB System Memory

* 15GB unallocated drive space

Users of system equipped with the minimum memory of 2GB may want to consider Fedora Spins with less resource intense Desktop Environments

.Low memory installations
[NOTE]
====
Fedora {PRODVER} can be installed and used on systems with limited resources for some applications. Text, VNC, or kickstart installations are advised over graphical installation for systems with very low memory. Larger package sets require more memory during installation, so users with less than 768MB of system memory may have better results performing a minimal install and adding to it afterward.

For best results on systems with less than 1GB of memory, use the DVD installation image.
====
== Recommended System Configuration

The figures below are recommended for the default x86_64 Workstation installation featuring the Gnome desktop . Your requirements may differ, depending on Desktop Environment and use case.

* 2GHz quad core processor

* 4GB System Memory

* 20GB unallocated drive space

.Low memory installations
[NOTE]
====
Fedora {PRODVER} can be installed and used on systems with limited resources for some applications. Text, VNC, or kickstart installations are advised over graphical installation for systems with very low memory. Larger package sets require more memory during installation, so users with less than 768MB of system memory may have better results preforming a minimal install and adding to it afterward.

For best results on systems with less than 1GB of memory, use the DVD installation image.
====

[[hardware_overview-resolution]]
== Display resolution

VGA capable of 1024x768 screen resolution

.Graphical Installation requires 800x600 resolution or higher
[NOTE]
====
Graphical installation of Fedora requires a minimum screen resolution of 800x600. Owners of devices with lower resolution, such as some netbooks, should use text or VNC installation.

Once installed, Fedora will support these lower resolution devices. The minimum resolution requirement applies only to graphical installation.
====

[[hardware_overview-graphics]]
== Graphics Hardware

[[hardware_overview-graphics-legacy_gpus]]
=== Minimum Hardware for Accelerated Desktops

Fedora {PRODVER} supports most display adapters. Modern, feature-rich desktop environments like *GNOME3* and *KDE Plasma Workspaces* use video devices to provide 3D-accelerated desktops. Older graphics hardware may *not support* acceleration:

* Intel prior to GMA9xx

* NVIDIA prior to NV30 (GeForce FX5xxx series)

* Radeon prior to R300 (Radeon 9500)

[[hardware_overview-graphics-cpu_acceleration]]
=== CPU Accelerated Graphics

Systems with older or no graphics acceleration devices can have accelerated desktop environments using *LLVMpipe* technology, which uses the CPU to render graphics. *LLVMpipe* requires a processor with `SSE2` extensions. The extensions supported by your processor are listed in the `flags:` section of `/proc/cpuinfo`

[[hardware_overview-graphics-desktops]]
=== Choosing a Desktop Environment for your hardware

Fedora {PRODVER}'s default desktop environment, *GNOME3*, functions best with hardware acceleration. Alternative desktops are recommended for users with older graphics hardware or those seeing insufficient performance with *LLVMpipe*.

Desktop environments can be added to an existing installation and selected at login. To list the available desktops, use the [command]`dnf environment list` command:

----
# dnf environment list
----

Install the desired environment using its ID as listed in the above command's output:

----
# dnf install @cinnamon-desktop-environment
----

For more information, see the `dnf5-environment` man page.
